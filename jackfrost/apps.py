from django.apps import AppConfig


class JackfrostConfig(AppConfig):
    name = 'jackfrost'
