from django.db import models

# Create your models here.
class CurrentConditions(models.Model):

    location = models.CharField(max_length=200)
    temp_f = models.IntegerField(default=0)
    wind_string = models.CharField(max_length=200)
    observation_time = models.CharField(max_length=200)
    url_history = models.CharField(max_length=200)

class WeatherHistory(models.Model):

    date = models.DateTimeField("observation date")
    temp_f = models.FloatField(default=0.0)
